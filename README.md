# Okruhy pro SZZ

* [Okruh 1](okruh1/otazky.pdf)
  * [Formální jazyky a automaty](https://github.com/martinrotter/fjaa-prednasky)
* Okruh 2
  * [OOT](https://drive.google.com/file/d/1VSZZ7q1bPx2WeuHJBvYe1llbcbpQjnA2/view)
  * [BEPS](https://docs.google.com/document/d/1Ynmaigd0fuLO12aRWMqklA27_GHhFKcak7IT_EKVCDw/edit#heading=h.k2vr113y6yr3)
  * [MWEB](https://drive.google.com/file/d/1IBPzZevUV4WVL_D_KHi5KSwRtwKzQ2O4/view)
* Okruh 3
  * [PDS]()
  * [MUSY](https://docs.google.com/document/d/1BUKgVzDTCzxc6ycnSdnXnRtrJhgks_c-keEbjfeFUu8/edit)
  * ALS1
    * [Hilbertovy stromy](okruh3/hilbertovy-stromy.pdf)
* Okruh 4
  * [KOM](https://docs.google.com/document/d/19MNziLDP3-RIAdcTZGI7YQSqw9dAtnlE4-iOadSQXEs/edit?usp=sharing)
  * [AZO](https://docs.google.com/document/d/1YahQefOsneqFINExaMo6tmE6hZH9SnOj3OBvvpaRywE/edit?usp=sharing)
  * [TMA](https://docs.google.com/document/d/1agi7Q1KGn4IHqWBdWV2j3wk3EiAYWaahBJn9nSdTazw/edit)
  * [PJA](https://docs.google.com/document/d/1Yv5bPWgcOE1tCgXOYTv60oLMUNkPngA_YRkQXe_g5ps/edit)

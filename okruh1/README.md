Potřebné balíčky ke kompilaci (případně další – sleduj `\usepackage{...}`):
```
texlive-gmp
texlive-bbm
texlive-bbm-macros
```

Pro formátování zdrojových kódů je použit balíček minted. Pro ten [je třeba mít nainstalované závislosti](https://github.com/gpoore/minted/blob/master/source/minted.pdf) pro Python. Instalace pro Fedoru:
```
texlive-framed
texlive-minted
texlive-simurgh
dnf install python*-pygments
```

Pro vývojové diagramy je použita přibalená knihovna pro tikz [tikz-uml](https://perso.ensta-paristech.fr/~kielbasi/tikzuml/var/files/doc/tikzumlmanual.pdf).
